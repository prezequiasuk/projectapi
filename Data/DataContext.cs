﻿using Microsoft.EntityFrameworkCore;
using TogetherMoneyTask.Models;

namespace TogetherMoneyTask.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<User> User { get; set; }
        public DbSet<Product> Product { get; set; }
    }
}
