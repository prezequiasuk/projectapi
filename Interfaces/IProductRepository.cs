﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TogetherMoneyTask.Models;

namespace TogetherMoneyTask.Interfaces
{
    public interface IProductRepository
    {
        List<Product> GetAllSoldProductsByPostcode(string Postcode);
        CoordinatesDTO GetCoodinates(string Postcode, string googleUrl, string apiKey);
    }
}
