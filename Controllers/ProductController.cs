﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Linq;
using TogetherMoneyTask.Interfaces;
using TogetherMoneyTask.Models;
using TogetherMoneyTask.Models.DTO;

namespace TogetherMoneyTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductRepository _productRepository;
        private readonly AppSettings _appSettings;
        public ProductController(IOptions<AppSettings> appSettingAcessor, IProductRepository productRepository)
        {
           this._productRepository = productRepository;
            this._appSettings = appSettingAcessor.Value;
        }

        [Route("GetListProduct/{Postcode}")]
        [HttpGet]
        public IActionResult GetAllSoldProductsByPostcode(string Postcode)
        {
            var response = new ProductResponseDTO();
            var apiKey = $"{_appSettings.GoogleApiKey}";
            var googleUrl = $"{_appSettings.GooglelUrl}";

            // -- for Unit Test Purpose only  --
            //var apiKey = $"Your Google api key";
            //var googleUrl = $"https://maps.googleapis.com/maps/api/geocode/xml?components=postal_code:";

            //Get Product list 
            var productList = _productRepository.GetAllSoldProductsByPostcode(Postcode);

            if (productList.Count() > 0)
            {
                var coordinates = _productRepository.GetCoodinates(Postcode, googleUrl, apiKey);
                if (coordinates != null)
                {
                    //Only display the Product details if coordinates are valid
                    response.Coordinate = coordinates;
                    response.ProductList = productList;

                    return Ok(response);
                }

                return BadRequest($"Cannot find the location for the provided Postcode: {Postcode}");
            }

            var errorMessage = $"There is not any list of sold product for the Postcode: {Postcode} at the moment";
            return BadRequest(new { errorMessage });
        }
    }
}