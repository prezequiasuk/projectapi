﻿namespace TogetherMoneyTask.Models
{
    public class CoordinatesDTO
    {
        public double lat { get; set; }
        public double lng { get; set; }

    }
}
