﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TogetherMoneyTask.Models.DTO
{
    public class ProductResponseDTO
    {
        public List<Product> ProductList { get; set; }
        public CoordinatesDTO Coordinate { get; set; }
    }
}
