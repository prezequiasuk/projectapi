﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TogetherMoneyTask.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public string Postcode { get; set; }

        public ICollection<Product> Product { get; set; }
    }
}
