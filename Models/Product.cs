﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TogetherMoneyTask.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public int? UserId { get; set; }
        public string ProductDescription { get; set; }
        public decimal Price { get; set; }
        public bool? Sold { get; set; }

        public User User { get; set; }
    }
}
