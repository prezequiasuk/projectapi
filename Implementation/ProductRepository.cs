﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml;
using TogetherMoneyTask.Data;
using TogetherMoneyTask.Interfaces;
using TogetherMoneyTask.Models;

namespace TogetherMoneyTask.Implementation
{
    public class ProductRepository : IProductRepository
    {
        private DataContext _context;
        public ProductRepository(DataContext context)
        {
            _context = context;
        }
        public List<Product> GetAllSoldProductsByPostcode(string Postcode)
        {
            //Get the last 5 sold products with their prices
            var productList = _context.Product.Include(x => x.User).Where(x => x.Sold == true && x.User.Postcode == Postcode).Take(5).ToList();

            return productList;
        }

        public CoordinatesDTO GetCoodinates(string Postcode, string googleUrl, string apiKey)
        {
            try
            {
                var lat = "";
                var lng = "";
                //Get the Coodinates lat and lng from google map API
                var googleMapApi = $"{googleUrl}{Postcode.Trim()}&sensor=false,Ca&key={apiKey}";
                if (!string.IsNullOrEmpty(googleMapApi))
                {
                    var result = new WebClient().DownloadString(googleMapApi);
                    var doc = new XmlDocument();
                    doc.LoadXml(result);

                    XmlNodeList ParentNode = doc.GetElementsByTagName("location");

                    foreach (XmlNode childrenNode in ParentNode)
                    {
                        lat = childrenNode.SelectSingleNode("lat").InnerText;
                        lng = childrenNode.SelectSingleNode("lng").InnerText;
                    }

                    var coordinates = new CoordinatesDTO()
                    {
                        lat = Convert.ToDouble(lat),
                        lng = Convert.ToDouble(lng)
                    };

                    return coordinates;
                }
                return null;
            }
            catch (Exception ex)
            {
                
                
                throw new Exception(ex.ToString());
            }
   
        }
    }
}
